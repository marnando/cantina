#ifndef COMPRAS_H
#define COMPRAS_H

#include <QObject>
#include <QQmlApplicationEngine>

class Produto;
class SearchProduto;
class Compras : public QObject
{
    Q_OBJECT
public:
    QQmlApplicationEngine m_engine;
    Produto * m_produto;
    SearchProduto * m_searchProduto;
public:
    explicit Compras(QObject *parent = nullptr);
    void loadApplication();

    Q_INVOKABLE bool insertCompra(QStringList binds);

private:
    void setRootContext();

signals:

public slots:
};

#endif // COMPRAS_H
