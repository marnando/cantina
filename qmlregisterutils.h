#ifndef QMLREGISTERUTILS_H
#define QMLREGISTERUTILS_H

#include <QtQml>

enum QmlPopupClosePolicy {
    NoAutoClose = 0,
    CloseOnPressOutside = 1,
    CloseOnPressOutsideParent = 2,
    CloseOnReleaseOutside = 4,
    CloseOnReleaseOutsideParent = 8,
    CloseOnEscape = 16
};

#define REGISTER_QML_MODEL(_name)\
void qmlRegister##_name() {\
    qmlRegisterType<_name>("Models", 1, 0, #_name);\
}\
Q_COREAPP_STARTUP_FUNCTION(qmlRegister##_name)

#define REGISTER_QML_UNCREATEBLE_MODEL(_name, _reason)\
void qmlRegisterUncreatable##_name() {\
    qmlRegisterUncreatableType<_name>("Models", 1, 0, #_name, _reason);\
}\
Q_COREAPP_STARTUP_FUNCTION(qmlRegisterUncreatable##_name)

#define REGISTER_QML_CONTROLLER(_name)\
void qmlRegister##_name() {\
    qmlRegisterType<_name>("Controllers", 1, 0, #_name);\
}\
Q_COREAPP_STARTUP_FUNCTION(qmlRegister##_name)

#endif // QMLREGISTERUTILS_H
