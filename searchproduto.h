#ifndef SEARCHPRODUTO_H
#define SEARCHPRODUTO_H

#include <QObject>
#include <QSqlRelationalTableModel>

namespace Produtos {
    static const QString TABLE_NAME = "produto";
    static const QString COLUMN_CODE = "codigo";
    static const QString COLUMN_DESCRICAO = "descricao";
    static const QString COLUMN_VALOR = "valor";
}

class SearchProduto : public QSqlRelationalTableModel
{
    Q_OBJECT
public:

    explicit SearchProduto(QObject *parent = nullptr);

    bool select();
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const {	return m_roleNames;	}
private:
    void generateRoleNames();
    QHash<int, QByteArray> m_roleNames;

signals:

public slots:
    void setFilter(QString text);
};

#endif // SEARCHPRODUTO_H
