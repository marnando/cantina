#ifndef CONNECTION_H
#define CONNECTION_H

#include <QObject>
#include <QDir>
#include <QSqlDatabase>
#include <QStandardItemModel>

class QSqlTableModel;
class Connection : public QObject
{
    Q_OBJECT
public:
    explicit Connection(QObject *parent = 0);
    ~Connection();

    bool insert(QString table, QStringList columnsNames, QStringList binds);

//Static functions
    static bool createConnection();
    static bool createTables();
    static QDir defaultDataDir();
    static QSqlDatabase* instanceDB();
    static void release();

private:
    static QSqlDatabase * m_db;

signals:
    void message(QString, QString);

public slots:
};

#endif // CONNECTION_H
