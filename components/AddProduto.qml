import QtQml 2.2
import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick.Window 2.3
import "../style"
import "base"

Dialog {
    id: addProdutoWindow
    height: parent.height * 0.4
    width: parent.width * 0.7
    property bool isUpdate: false
    innerObject: Rectangle {
        anchors.fill: parent
        ColumnLayout {
            spacing: 16
            width: parent.width
            anchors.fill: parent

            Item {
                Layout.fillHeight: true
            }

            TextField {
                id: produto
                placeholderText: qsTr("Digite o nome do produto")
            }

            TextField {
                id: prodValor
                placeholderText: qsTr("Digite o valor do produto")
            }

            Item {
                Layout.fillHeight: true
            }
        }
    }

    onDialogConfirm: {
        if (produto.text.length > 0
         && prodValor.text.length > 0) {
            if (isUpdate) {
                Produtos.append([produto.text,
                                 prodValor.text,
                                 Math.round(new Date() / 1000)]);
            }
        } else {
            console.log("Campos obrigatórios não preenchidos")
        }
    }

    onDialogCancel: {
        console.log("Dialog cancelado")
    }

    Component.onCompleted: {
        isUpdate = produto.text.length > 0 && prodValor.text.length > 0
    }

    function setData(descricao, valor) {
        produto.text = descricao;
        prodValor.text = valor;
    }
}
