import QtQuick 2.0
import QtQuick.Controls 2.2

Button {
    id: borderlessButton
    background: Rectangle {
        border.color: borderlessButton.down ? "green" : "transparent"
    }
}

