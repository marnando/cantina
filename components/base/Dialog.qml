import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import "../base"
import "../../style"

BasePopup {
    id: dialogPopup
    property alias innerObject: contentDialog.children
    property alias hideCancelButton: btnCancel.visible
    property alias hideConfirmButton: btnApply.visible
    signal dialogConfirm
    signal dialogCancel

    ColumnLayout {
        id: contentDialog
        anchors {
            top: parent.top
            bottom: dialogButtons.top
            bottomMargin: 16
        }
    }

    RowLayout {
        id: dialogButtons
        spacing: 16
        anchors {
            bottom: parent.bottom
            bottomMargin: 16
        }

        Item {
            id: spacer1
            Layout.fillWidth: true
        }

        Button {
            id: btnCancel
            text: "Cancelar"
            font.pixelSize: Metrics.fontMetric
            onClicked: {
                dialogPopup.dialogCancel();
                close();
            }
        }

        Button {
            id: btnApply
            text: "Confirmar"
            font.pixelSize: Metrics.fontMetric
            onClicked: {
                dialogPopup.dialogConfirm();
                close();
            }
        }
    }
}
