import QtQuick 2.0

ListView {
    id: listaBusca
    focus: true
    highlightRangeMode: ListView.ApplyRange
    highlight: Component {
        id: highlight
        Rectangle {
            width: listaBusca.width - 1
            height: listaBusca.currentItem.height - 1
            color: "LightSteelBlue"
            border.color: "gray"
            radius: 5
            opacity: 0.5
            z: 10
            y: listaBusca.currentItem.y
            Behavior on y {
                SpringAnimation {
                    spring: 3
                    damping: 0.2
                }
            }
        }
    }
}
