import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Window 2.3

Popup {
    id: basePopupWidget
    x: (parent.width - width) / 2
    y: (parent.height - height) / 2
    height: parent.height * 0.8
    width: parent.width * 0.7
    modal: true
    focus: true
    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
}
