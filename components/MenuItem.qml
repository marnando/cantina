import QtQuick 2.0
import QtQuick.Controls 2.2
import "base"

Menu {
    id: contactMenu
    x: parent.width / 2 - width / 2
    y: parent.height / 2 - height / 2
    modal: true

    Label {
        padding: 10
        font.bold: true
        width: parent.width
        horizontalAlignment: Qt.AlignHCenter
//        text: currentContact >= 0 ? contactView.model.get(currentContact).fullName : ""
    }
    MenuItem {
        text: qsTr("Edit...")
        onTriggered: contactDialog.editContact(contactView.model.get(currentContact))
    }
    MenuItem {
        text: qsTr("Remove")
        onTriggered: contactView.model.remove(currentContact)
    }
}
