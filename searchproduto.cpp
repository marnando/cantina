#include "searchproduto.h"
#include "connection.h"
#include <QSqlQuery>
#include <QSqlRecord>

SearchProduto::SearchProduto(QObject *parent)
    : QSqlRelationalTableModel(parent
                             , Connection::instanceDB()->database())
{
    setTable("compras");
}

bool SearchProduto::select()
{
    bool retorno = QSqlRelationalTableModel::select();

    if (retorno) {
        generateRoleNames();
    }

    return retorno;
}

QVariant SearchProduto::data(const QModelIndex &index, int role) const
{
    QVariant value;
    if(role < Qt::UserRole) {
        value = QSqlQueryModel::data(index, role);
    }
    else {
        int columnIdx = role - Qt::UserRole - 1;
        QModelIndex modelIndex = this->index(index.row(), columnIdx);
        value = QSqlQueryModel::data(modelIndex, Qt::DisplayRole);
    }
    return value;
}

void SearchProduto::generateRoleNames()
{
    m_roleNames.clear();
    for( int i = 0; i < record().count(); i ++) {
        m_roleNames.insert(Qt::UserRole + i + 1, record().fieldName(i).toUtf8());
    }
}

void SearchProduto::setFilter(QString text)
{
    text = (text.isEmpty() ? "#$#" : text);
    QString filter = "    descricao LIKE '%" + text + "%' "
                     " OR valor LIKE '%" + text + "%' ";
    beginResetModel();
    QSqlRelationalTableModel::setFilter(filter);
    QSqlRelationalTableModel::setSort(fieldIndex("descricao"), Qt::AscendingOrder);
    select();
    endResetModel();
}
