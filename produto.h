#ifndef PRODUTO_H
#define PRODUTO_H

#include <QObject>
#include <QSqlQueryModel>

class Produto : public QSqlQueryModel
{
    Q_OBJECT
public:
    enum ProductModel {
        Codigo = Qt::UserRole + 1,
        Descricao,
        Valor
    };

    explicit Produto(QObject *parent = nullptr);

    void setQuery();
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const { return m_roleNames; }
    bool setData(const QModelIndex &item, const QVariant &value, int role);

    Q_INVOKABLE QString code(int row);
    Q_INVOKABLE QString descricao(int row);
    Q_INVOKABLE QString valor(int row);
private:
    void generateRoleNames();
    QHash<int, QByteArray> m_roleNames;

signals:
    void codigoChanged();

public slots:
    void append(QStringList binds);
    void update(QStringList binds);
    void remove(int row);
};

#endif // PRODUTO_H
