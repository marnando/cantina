#include "connection.h"
#include <QSqlError>
#include <QGuiApplication>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QSqlTableModel>
#include <QDir>
#include <QStandardPaths>

QSqlDatabase * Connection::m_db = nullptr;
Connection::Connection(QObject *parent) : QObject(parent)
{

}

Connection::~Connection()
{

}

bool Connection::insert(QString table, QStringList columnsNames, QStringList binds)
{
    QString sql = " INSERT OR REPLACE INTO " + table + " ( ";
    QString cols = "";
    int cont = 0;
    foreach (QString colName, columnsNames) {
        cols += colName;
        cont++;
        if (cont < columnsNames.size()) {
            cols += colName + " , ";
        }
    }

    sql += sql + cols;
    sql += " ) VALUES ( ";

    for (int i = 1; i <= binds.size(); ++i) {
        sql += "?" + QString::number(i);
        if (i < binds.size()) {
            sql += " , ";
        }
        cont++;
    }

    sql += " ) ; ";

    cont = 1;
    QSqlQuery query = QSqlQuery(m_db->database());
    query.prepare(sql);
    foreach (QString bind, binds) {
        query.bindValue("?" + QString::number(cont), bind);
    }

    return query.exec();
}

QDir Connection::defaultDataDir()
{
#ifdef Q_OS_ANDROID
    QDir standardDir = QDir(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation));
    standardDir.cdUp();

    QString dataPath = standardDir.absolutePath() + "/varejopdv/data";
    QDir dataDir(dataPath);
    if (!dataDir.exists()) {
        dataDir.mkpath(dataPath);
    }
    return dataDir;
#else
    QString dataPath = qApp->applicationDirPath() + "/data";
    QDir dataDir(dataPath);
    if (!dataDir.exists()) {
        dataDir.mkpath(dataPath);
    }
    return dataDir;
#endif
}

bool Connection::createConnection()
{
    // Conecto o database
    m_db = new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE"));
    QDir dataDir = defaultDataDir();

    // Seto um nome para o meu database
    m_db->setDatabaseName(dataDir.absolutePath() + "/" + "cantina" + ".db3");

    // Verifico se consigo carregá-lo
    if (!m_db->open()) {
//        emit message(tr("Não pode abrir um Database")
//                   , tr("Não há uma conexão de estabilidade para um database.\n"
//                        "Esse exemplo precisa de suporte ao SQLite. Por favor leia "
//                        "a documentation Qt SQL driver para informações de como "
//                        "executá-lo.\n\n"
//                        "Clique em cancelar para finalizar."));
        return false;
    }

    createTables();
    return true;
}

bool Connection::createTables()
{
    QSqlQuery query = QSqlQuery(Connection::instanceDB()->database());
    QString sql;

    sql = " CREATE TABLE IF NOT EXISTS compras ( "
          "        codigo INTEGER PRIMARY KEY AUTOINCREMENT, "
          "        descricao TEXT, "
          "        valor REAL, "
          "        data INTEGER "
          " ); ";
    query.prepare(sql);
    query.exec();

    query.finish();
    query.clear();

    sql = " CREATE TABLE IF NOT EXISTS produtos ( "
          "        codigo INTEGER PRIMARY KEY AUTOINCREMENT, "
          "        descricao TEXT,  "
          "        valor REAL "
          " ); ";

    query.prepare(sql);
    query.exec();
}

QSqlDatabase *Connection::instanceDB()
{
    if (!m_db)
    {
       createConnection();
    }

    return m_db;
}

void Connection::release()
{
    if (m_db)
    {
        m_db->close();
        delete m_db;
        m_db = nullptr;
    }
}
