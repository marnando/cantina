import QtQuick 2.9

pragma Singleton

QtObject {
    id: uiUtils

    function showDialog(parent, qmlFile) {
        var component = Qt.createComponent(qmlFile);
        if (component.status === Component.Ready) {
            var popup = component.createObject(parent);
            if (popup) {
                popup.open();
            }
        } else {
            console.log("Dialog " + qmlFile + " is null: ", Component.errorString);
        }

        return component;
    }
}
