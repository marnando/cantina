import QtQuick 2.9

pragma Singleton

QtObject {
    id: uiMetrics

    // Operational Systems
    readonly property int windows: 0
    readonly property int unix: 1
    readonly property int androidS: 2
    readonly property int macOs: 3

    readonly property int fontMetric: 20
    readonly property int heightBtn: 32
    readonly property int widthBtn: 32
}
