#include "compras.h"
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QStyleHints>
#include <QQuickStyle>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    QQuickStyle::setStyle(QStringLiteral("qrc:/style"));
    Compras compras;
    compras.loadApplication();
    return app.exec();
}
