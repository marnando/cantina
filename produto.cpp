#include "produto.h"
#include "connection.h"
#include "qmlregisterutils.h"
#include <QSqlQuery>
#include <QSqlRecord>

Produto::Produto(QObject *parent) : QSqlQueryModel(parent)
{
    this->setQuery();
}

void Produto::setQuery()
{
    QSqlQueryModel::setQuery(" SELECT * FROM compras ; ", Connection::instanceDB()->database());
    generateRoleNames();
}

QVariant Produto::data(const QModelIndex &index, int role) const
{
    QVariant value;

    if(role < Qt::UserRole) {
        value = QSqlQueryModel::data(index, role);
    }
    else {
        int columnIdx = role - Qt::UserRole - 1;
        QModelIndex modelIndex = this->index(index.row(), columnIdx);
        value = QSqlQueryModel::data(modelIndex, Qt::DisplayRole);
    }

    return value;
}

bool Produto::setData(const QModelIndex &item, const QVariant &value, int role)
{
    if (item.isValid() && role == Qt::EditRole) {
        QSqlQueryModel::setData(item, value,role);
        emit dataChanged(item, item);
        return true;
    }
    return false;
}

QString Produto::code(int row)
{
    return data(index(row, 0), Codigo).toString();
}

QString Produto::descricao(int row)
{
    return data(index(row, 0), Descricao).toString();
}

QString Produto::valor(int row)
{
    return data(index(row, 0), Valor).toString();
}

void Produto::append(QStringList binds)
{
    QString sql = " INSERT OR REPLACE "
                  "   INTO compras (codigo, descricao, valor, data) "
                  " VALUES (:codigo, :descricao, :valor, :data) ; ";
    QSqlQuery query = QSqlQuery(Connection::instanceDB()->database());
    query.prepare(sql);
    query.bindValue(":codigo", binds.at(0));
    query.bindValue(":descricao", binds.at(1));
    query.bindValue(":valor", binds.at(2));
    query.bindValue(":data", binds.at(3));
    query.exec();
    setQuery();
}

void Produto::update(QStringList binds)
{
    QString sql = " UPDATE compras SET descricao = :descricao, valor = :valor WHERE codigo = :codigo ; ";
    QSqlQuery query = QSqlQuery(Connection::instanceDB()->database());
    query.prepare(sql);
    query.bindValue(":codigo", binds.at(0));
    query.bindValue(":descricao", binds.at(1));
    query.bindValue(":valor", binds.at(2));
    query.exec();
    setQuery();
}

void Produto::remove(int row)
{
    QString sql = " DELETE FROM compras WHERE codigo = :codigo ; ";
    QSqlQuery query = QSqlQuery(Connection::instanceDB()->database());
    query.prepare(sql);
    query.bindValue(":codigo", code(row).toInt());
    query.exec();
    setQuery();
}

void Produto::generateRoleNames()
{
    m_roleNames.clear();
    for( int i = 0; i < record().count(); i ++) {
        m_roleNames.insert(Qt::UserRole + i + 1, record().fieldName(i).toUtf8());
    }
}

//REGISTER_QML_MODEL(Produto)
