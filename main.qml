import QtQuick 2.9
import QtQuick.Window 2.3
import QtQuick.Controls 2.2 as Controls
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import "components"
import "style"

Controls.ApplicationWindow {
    id: root
    visible: true
    width: 400
    height: 700
    title: qsTr("Cantina")

    property alias listBuscaModel: listaBusca.model
    property int currentProduct: -1

    RowLayout {
        id: menuLayout
        height: 50
        width: parent.width
        anchors.top: parent.top
        Item {
            Layout.fillWidth:true
        }

        Controls.Button {
            id: novoItem
            Layout.fillHeight: true
            text: qsTr("Item novo")
            font.pixelSize: Metrics.fontMetric
            onClicked: {
                UiUtils.showDialog(root, "qrc:/components/AddProduto.qml");
            }
        }
    }

    RowLayout {
        id: campoBusca
        height: 50
        width: parent.width
        anchors {
            top: menuLayout.bottom
            topMargin: 16
        }

        Rectangle {
            anchors.fill: parent
            color: "transparent"
            border.color: "gray"
        }

        Controls.TextField {
            id: textoBusca
            Layout.fillWidth: true
            font.pixelSize: Metrics.fontMetric
            placeholderText: qsTr("Digite um item pra buscar")
            background: Rectangle { border.color: "transparent" }
            onTextEdited: {
                BuscaProdutos.setFilter(textoBusca.text)
            }
        }

        Controls.Button {
            id: busca
            Layout.fillWidth: true
            Layout.fillHeight: true
            implicitHeight: Metrics.heightBtn
            implicitWidth: Metrics.widthBtn
            background: Rectangle {
                color: busca.down ? "lightsteelblue" : "transparent"
            }
            Image {
                id: lupa
                anchors.fill: parent
                source: "qrc:/images/icon_magnify_glass.png"
                fillMode: Image.PreserveAspectFit
            }
            onClicked: {
                listaBusca.visible = true
            }
        }
    }

    ListView {
        id: listaBusca
        height: 200
        width: parent.width
        focus: true
        highlightRangeMode: ListView.ApplyRange
        highlight: Component {
            Rectangle {
                width: listaBusca.width - 1
                height: listaBusca.currentItem.height - 1
                color: "transparent"
                border.color: "gray"
                radius: 5
                z: 10
            }
        }
        anchors {
            top: campoBusca.bottom
            topMargin: 16
        }

        model: BuscaProdutos
        delegate: Rectangle {
            width: listaBusca.width
            height: 50

            RowLayout {
                anchors.fill: parent

                Column {
                    spacing: 1
                    Text {
                        id: productText
                        text: descricao
                        font.pixelSize: Metrics.fontMetric
                    }
                    Text {
                        id: productPrice
                        text: "R$ " + valor.toFixed(2);
                        font.pixelSize: Metrics.fontMetric / 1.5
                    }
                }

                Item {
                    Layout.fillWidth: true
                }

                Controls.Button {
                    id: addIcon
                    z: 100
                    implicitHeight: Metrics.heightBtn
                    implicitWidth: Metrics.widthBtn
                    background: Rectangle {
                        color: busca.down ? "lightsteelblue" : "transparent"
                    }

                    Image {
                        anchors.fill: parent
                        source: "qrc:/images/add.png"
                        fillMode: Image.PreserveAspectFit
                    }

                    onClicked: {
                        console.log("Text")
                    }
                }
            }

            MouseArea {
                anchors.fill: parent
                onClicked: listaBusca.currentIndex = index
            }
        }
    }

    Controls.Menu {
        id: contactMenu
        x: parent.width / 2 - width / 2
        y: parent.height / 2 - height / 2
        modal: true

        Controls.Label {
            padding: 10
            font.bold: true
            width: parent.width
            horizontalAlignment: Qt.AlignHCenter
            text: currentProduct >= 0 ? Produtos.descricao(currentProduct) : ""
        }

        Controls.MenuItem {
            id: menuEditar
            text: qsTr("Editar")
            onTriggered: {
            }
        }

        Controls.MenuItem {
            id: menuRemover
            text: qsTr("Remover")
            onTriggered: {
                Produtos.remove(currentProduct);
            }
        }
    }

    Rectangle {
//        border.color: "gray"
        width: parent.width
        anchors.top: listaBusca.bottom
        anchors.bottom: footerLayout.top

        ColumnLayout {
            anchors.fill: parent
            spacing: 16
            Controls.Label {
                text: qsTr("Lista de compras")
                font.pixelSize: Metrics.fontMetric
            }

            ListView {
                id: listaCompra
                focus: true
                Layout.fillHeight: true
                Layout.fillWidth: true
                currentIndex: 0
                highlightRangeMode: ListView.ApplyRange
                highlight: Component {
                    Rectangle {
                        width: listaCompra.width - 1
                        height: listaCompra.currentItem.height - 1
                        color: UIStyles.transparent
                        border.color: UIStyles.colorLightSteelBlue
                        border.width: 3
                        z: 10
                    }
                }
                model: Produtos
                delegate: Rectangle {
                    width: listaCompra.width;
                    height: 50
//                    color: index % 2 == 0 ? "#81D4FA" : "transparent"
                    //Card
                    Controls.Label {
                        text: model.descricao
                        font.pixelSize: Metrics.fontMetric
                        Layout.alignment: Text.AlignVCenter
                    }

//                    Rectangle {
//                        color: "#607D8B"
//                        width: parent.width
//                        height: 1
//                        anchors.bottom: parent.bottom
//                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            listaCompra.currentIndex = index;
                        }

                        onPressAndHold: {
                            currentProduct = index
                            contactMenu.open()
                        }
                    }
                }
            }
        }
    }

    RowLayout {
        id: footerLayout
        height: 50
        width: parent.width
        anchors {
            bottom: parent.bottom
            bottomMargin: 16
        }

        Item { Layout.fillWidth: true }
        Controls.Button {
            text: "Teste"
        }
        Item { Layout.fillWidth: true }
    }
}
