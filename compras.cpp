#include "compras.h"
#include "compras.h"
#include "connection.h"
#include "produto.h"
#include "searchproduto.h"
#include <QQmlContext>
#include <QSqlQuery>
#include <QSqlTableModel>

Compras::Compras(QObject *parent)
    : QObject(parent)
    , m_engine(new QQmlApplicationEngine(this))
    , m_produto(nullptr)
    , m_searchProduto(nullptr)
{
    setRootContext();
    m_engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    Connection::instanceDB();
}

void Compras::loadApplication()
{
    m_engine.rootObjects().isEmpty();
}

bool Compras::insertCompra(QStringList binds)
{
    QString sql = " INSERT INTO compras (descricao, valor, data) VALUES (:descricao, :valor, :data) ; ";
    QSqlQuery query = QSqlQuery(Connection::instanceDB()->database());
    query.prepare(sql);
    query.bindValue(":descricao", binds.at(0));
    query.bindValue(":valor", binds.at(1));
    query.bindValue(":data", binds.at(2));
    return query.exec();
}

void Compras::setRootContext()
{
    m_produto = new Produto(this);
    m_searchProduto = new SearchProduto(this);
    m_engine.rootContext()->setContextProperty("Compras", this);
    m_engine.rootContext()->setContextProperty("Produtos", m_produto);
    m_engine.rootContext()->setContextProperty("BuscaProdutos", m_searchProduto);
}
